#!/usr/bin/perl

use strict;
use warnings;

while (<>) {
    chomp;
    my ($scientific_name, $taxid, $assembly_name, $date, $provider_name, $provider_url, $dir) = split /\t/;
    my $display_name    = "$scientific_name $assembly_name";
    my $production_name =  $display_name;
    $production_name =~ s/ /_/g ;
    my $db_name = lc($production_name) . "_core_27_80_1";
    open  INI, ">../2015-09-13-import-ini/$db_name.ini" or die $!;
    print INI  "
[ENSEMBL]
    ENSEMBL = https://github.com/Ensembl/ensembl.git
    PRODUCTION = https://github.com/Ensembl/ensembl-production.git
    BRANCH = release/80
    PIPELINE = https://github.com/Ensembl/ensembl-pipeline.git
    LOCAL = /scratch/skumar/ensemblgit
[DATABASE_CORE]
    NAME = $db_name
    HOST = flowers.bio.ed.ac.uk
    PORT = 3307
    RW_USER = lepbase-import
    RW_PASS = 
    RO_USER = ensro
    RO_PASS = password
[DATABASE_TAXONOMY]
    NAME = ncbi_taxonomy
    HOST = flowers.bio.ed.ac.uk
    PORT = 3307
    RO_USER = ensro
    RO_PASS = password
[DATABASE_TEMPLATE]
    NAME = heliconius_melpomene_core_27_80_1
    HOST = mysql-eg-publicsql.ebi.ac.uk
    PORT = 4157
    RO_USER = anonymous
    RO_PASS = password
[META]
    SPECIES.PRODUCTION_NAME = $production_name
    SPECIES.SCIENTIFIC_NAME = $scientific_name
    SPECIES.COMMON_NAME = 
    SPECIES.DISPLAY_NAME = $display_name
    SPECIES.DIVISION = EnsemblMetazoa
    SPECIES.URL = $production_name
    SPECIES.TAXONOMY_ID = $taxid
    SPECIES.ALIAS = [ ]
    ASSEMBLY.NAME = $assembly_name
    ASSEMBLY.DATE = $date
    ASSEMBLY.ACCESSION = 
    ASSEMBLY.DEFAULT = $assembly_name
    PROVIDER.NAME = $provider_name
    PROVIDER.URL = $provider_url
    
    GENEBUILD.ID = 1
    GENEBUILD.START_DATE = 2015-09
    GENEBUILD.VERSION = 1
    GENEBUILD.METHOD = import
[GFF]
    SORT = 1
;   SPLIT = [ ##FASTA GFF CONTIG ]
    CHUNK = [ change region ]
;   CHUNK = [ separator             ### ]
    CONDITION1 = [ MULTILINE   cds ]
    CONDITION2 = [ EXPECTATION cds   hasSister exon force ]
    CONDITION3 = [ EXPECTATION cds   hasParent mrna force ];
    CONDITION4 = [ EXPECTATION exon  hasParent mrna|mirna|trna|rrna force ];
    CONDITION5 = [ EXPECTATION mrna  hasParent gene force ];
    CONDITION6 = [ EXPECTATION mirna hasParent gene force ];
    CONDITION7 = [ EXPECTATION rrna  hasParent gene force ];
    CONDITION8 = [ EXPECTATION trna  hasParent gene force ];
    CONDITION9 = [ EXPECTATION cds|exon|mrna|mirna|trna|rrna|gene <=[_start,_end] SELF warn ];
[FILES]
    SCAFFOLD = [ fa /exports/colossus/lepbase/heliconius18_blobexisting/$dir/$dir\.$assembly_name\.fasta ]
    GFF = [ gff3 dummy.gff ]
"
}