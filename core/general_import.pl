#!/usr/bin/perl -w

use strict;
use DBI;

## load parameters from an INI-style config file for maximal ensemblism
## check that all required parameters have been defined in the config file
my $ini_file = $ARGV[0] || die "ERROR: you must specify an ini file\n",usage(),"\n";
my $params = load_ini($ini_file);

## checkout ensembl repositories
clone_ensembl_code($params);

## connect to database host
my $dbh = core_db_host_connect($params);

## create database, load schema and populate meta tables etc.
## TODO: add taxonomy and interpro tables - see specify_genebuild.sh for details
##       only run this if an overwrite flag is passed on the command line
setup_core_db($dbh,$params);

## download/obtain files using methods suggested by file paths and extensions
my %infiles;
foreach my $subsection (keys %{$params->{'FILES'}}){
	($infiles{$subsection}{'name'},$infiles{$subsection}{'type'}) = fetch_file($params->{'FILES'}{$subsection});
}

## TODO: calculate scaffold stats and write to file
##       trigger manual intervention to deal with any anomalies

## load contigs/scaffolds and agp
## TODO: handle levels other than scaffold and contig
##       convert contig/scaffold names to standard format and load as synonyms
##       add check that the expected number of sequences have been loaded
load_sequences($params,\%infiles);

## get name/description information from fasta files if defined in the config
# descriptions are accessed from the locations lower down the list only if not already found
# 	[GENE_DESCRIPTIONS]
#		SOURCE = [ file.gff3 file.fa file.txt ]
#		SECTION = [ Gene->description DEFLINE 1 ]
#		REGEX  = [ /desc=([^;+])/ /(.+)/ /.+?\|(.+)/ ]
# 	[NAMES]
#		...

## check that gff file is properly formatted, repair problems that can be fixed and weave in names/descriptions
# include test for phase
# maybe standardise xrefs at this stage
# write out a checked and formatted gff file

### trigger manual intervention if the gff file cannot be loaded

## calculate summary statistics from the gff

### trigger manual intervention to deal with any anomalies

## use gff to generate fasta files of gene, transcript, protein, etc sequences
# use naming convention required by sequenceserver

## compare generated sequence files with any provider files and compare

### trigger manual intervention to deal with any anomalies

## read xref to ensembl mapping information from config file
# 	[XREFS]
#		IPRSCAN = IPR
#		UNIPROT = UP
#		...

## load gff into ensembl

### trigger manual intervention to deal with any anomalies

## load other xref files (iprscan, blastp, etc) into database
# check format before loading
# add descriptions if specified

### trigger manual intervention to deal with any anomalies

sub load_sequences {
	my ($params,$infiles,$dbh) = @_;
	my $connection_info = 	 '-dbuser '.$params->{'DATABASE_CORE'}{'RW_USER'}
							.' -dbhost '.$params->{'DATABASE_CORE'}{'HOST'}
							.' -dbport '.$params->{'DATABASE_CORE'}{'PORT'}
							.' -dbname '.$params->{'DATABASE_CORE'}{'NAME'}
							.' -dbpass '.$params->{'DATABASE_CORE'}{'RW_PASS'};
	my $perl_libs = $params->{'ENSEMBL'}{'LOCAL'}.'/ensembl/modules';
	my $perl = "perl -I $perl_libs";
	if ($infiles->{'CONTIG'} && !$infiles->{'SCAFFOLD'}){
		if ($infiles->{'CONTIG'}{'type'} !~ m/^(?:fa|faa|fas|fasta|fna)$/){
			die "ERROR: [FILES] CONTIG $infiles->{'CONTIG'}{'name'} must be a fasta file if it is the only sequence file specified\n";
		}
		system $perl.' '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline/scripts/load_seq_region.pl '.$connection_info.' -coord_system_name contig -rank 1 -coord_system_version '.$params->{'META'}{'ASSEMBLY.NAME'}.' -default_version -sequence_level -verbose -fasta_file '.$infiles->{'CONTIG'}{'name'}.' -replace_ambiguous_bases';
	}
	elsif ($infiles->{'CONTIG'}){
		if ($infiles->{'CONTIG'}{'type'} !~ m/^(?:fa|faa|fas|fasta|fna)$/ && $infiles->{'SCAFFOLD'}{'type'} !~ m/^(?:fa|faa|fas|fasta|fna)$/){
			die "ERROR: at least one of [FILES] CONTIG $infiles->{'CONTIG'}{'name'} or SCAFFOLD $infiles->{'SCAFFOLD'}{'name'} must be a fasta file\n";
		}
		if ($infiles->{'CONTIG'}{'type'} ne 'agp' && $infiles->{'SCAFFOLD'}{'type'} ne 'agp'){
			die "ERROR: at least one of [FILES] CONTIG $infiles->{'CONTIG'}{'name'} or SCAFFOLD $infiles->{'SCAFFOLD'}{'name'} must be agp file\n";
		}
		if ($infiles->{'CONTIG'}{'type'} ne 'agp'){
			system $perl.' '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline/scripts/load_seq_region.pl '.$connection_info.' -coord_system_name scaffold -rank 1 -coord_system_version '.$params->{'META'}{'ASSEMBLY.NAME'}.' -default_version -agp_file '.$infiles->{'SCAFFOLD'}{'name'};
			system $perl.' '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline/scripts/load_seq_region.pl '.$connection_info.' -coord_system_name contig -rank 2 -default_version -sequence_level -verbose -fasta_file '.$infiles->{'CONTIG'}{'name'}.' -replace_ambiguous_bases';
			system $perl.' '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline/scripts/load_agp.pl '.$connection_info.' -assembled_name scaffold -assembled_version '.$params->{'META'}{'ASSEMBLY.NAME'}.' -component_name contig -agp_file '.$infiles->{'SCAFFOLD'}{'name'};
		}
		else {
			system $perl.' '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline/scripts/load_seq_region.pl '.$connection_info.' -coord_system_name scaffold -rank 1 -default_version -sequence_level -verbose -fasta_file '.$infiles->{'SCAFFOLD'}{'name'}.' -replace_ambiguous_bases';
			system $perl.' '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline/scripts/load_seq_region.pl '.$connection_info.' -coord_system_name contig -rank 2 -coord_system_version '.$params->{'META'}{'ASSEMBLY.NAME'}.' -default_version -agp_file '.$infiles->{'CONTIG'}{'name'};
			system $perl.' '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline/scripts/load_agp.pl '.$connection_info.' -assembled_name contig -assembled_version '.$params->{'META'}{'ASSEMBLY.NAME'}.' -component_name scaffold -agp_file '.$infiles->{'CONTIG'}{'name'};
		}
		$dbh->do('INSERT INTO meta(species_id, meta_key,meta_value) VALUES (1, '.$dbh->quote('assembly.mapping').','.$dbh->quote('scaffold:'.$params->{'META'}{'ASSEMBLY.NAME'}.'|contig').')');
	}
	else {
		if ($infiles->{'SCAFFOLD'}{'type'} !~ m/^(?:fa|faa|fas|fasta|fna)$/){
			die "ERROR: [FILES] SCAFFOLD $infiles->{'SCAFFOLD'}{'name'} must be a fasta file if it is the only sequence file specified\n";
		}
		system $perl.' '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline/scripts/load_seq_region.pl '.$connection_info.' -coord_system_name scaffold -rank 1 -coord_system_version '.$params->{'META'}{'ASSEMBLY.NAME'}.' -default_version -sequence_level -verbose -fasta_file '.$infiles->{'SCAFFOLD'}{'name'}.' -replace_ambiguous_bases';

	}
	system $perl.' '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline/scripts/set_toplevel.pl '.$connection_info;
	
	return 1;

}

sub setup_core_db {
	my $dbh = shift;
	my $params = shift;
	
	$dbh->do('DROP DATABASE IF EXISTS '.$params->{'DATABASE_CORE'}{'NAME'}) || die "ERROR: unable to drop existing [DATABASE_CORE] NAME ".$params->{'DATABASE_CORE'}{'NAME'}." using provided settings";
	$dbh->do('CREATE DATABASE '.$params->{'DATABASE_CORE'}{'NAME'}) || die "ERROR: unable to create to [DATABASE_CORE] NAME ".$params->{'DATABASE_CORE'}{'NAME'}." using provided settings";
	
	my $file = $params->{'ENSEMBL'}{'LOCAL'}.'/ensembl/sql/table.sql';
	my $connection_info = 	 '-u'.$params->{'DATABASE_CORE'}{'RW_USER'}
							.' -h'.$params->{'DATABASE_CORE'}{'HOST'}
							.' -P'.$params->{'DATABASE_CORE'}{'PORT'}
							.' -D'.$params->{'DATABASE_CORE'}{'NAME'}
							.' -p'.$params->{'DATABASE_CORE'}{'RW_PASS'};
	system "mysql $connection_info < $file";
	
	$file = $params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline/sql/table.sql';
	system "mysql $connection_info < $file";
	
	$dbh->do('USE '.$params->{'DATABASE_CORE'}{'NAME'});

	## loop through [META] and load data
	foreach my $key (keys %{$params->{'META'}}){
		my $value = $params->{'META'}{$key};
		$key = 'species'.$key unless $key =~ m/\./;
		if (ref $value){
			for (my $i = 0; $i < @{$value}; $i++){
				my $tmp = $value->[$i];
				$tmp =~ tr/[A-Z]/[a-z]/;
				$dbh->do('INSERT INTO meta (meta_key,meta_value) VALUES ('.$dbh->quote($key).','.$dbh->quote($tmp).')');
			}
		}
		else {
			$value =~ tr/[A-Z]/[a-z]/;
			$dbh->do('INSERT INTO meta (meta_key,meta_value) VALUES ('.$dbh->quote($key).','.$dbh->quote($value).')');
		}
	}
	
	
	## TODO: add meta_key = taxonomy 
	##       add interpro tables 

	## populate production db tables
	system 	 'perl '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-production/scripts/production_database/populate_production_db_tables.pl'
			.' -h '.$params->{'DATABASE_CORE'}{'HOST'}
			.' -P '.$params->{'DATABASE_CORE'}{'PORT'}
			.' -u '.$params->{'DATABASE_CORE'}{'RW_USER'}
			.' -p '.$params->{'DATABASE_CORE'}{'RW_PASS'}
			.' -d '.$params->{'DATABASE_CORE'}{'NAME'}
			.' -mh '.$params->{'DATABASE_TEMPLATE'}{'HOST'}
			.' -mP '.$params->{'DATABASE_TEMPLATE'}{'PORT'}
			.' -md '.$params->{'DATABASE_TEMPLATE'}{'NAME'}
			.' -dp ./';

	## clean up sql backups
	system 'rm '.$params->{'DATABASE_CORE'}{'NAME'}.'*.sql';
	
	return 1;
}

sub fetch_file {
	my ($type,$location,$new_name);
	$location = shift;
	if (ref $location){
		$type = $location->[0];
		if (defined $location->[2]){
			$new_name = $location->[2];
		}
		$location = $location->[1];
	}
	# work out file name from location
	$location =~ m/.+\/([^\/]+)$/;
	my $filename = $1 ? $1 : $location;
	my $compression;
	if ($filename =~ s/\.(gz|gzip|tar\.gz|tgz|zip)$//){
		$compression = $1;
	}
	my $command;
	if (($new_name && !-e $new_name) || (!$new_name && !-e $filename)){
		if ($location =~ m/^(?:ftp|http|https):/){
			$command = 'wget';
			system "wget $location";
		}
		elsif ($location =~ m/:[\/~]/){
			$command = 'scp';
			system "scp $location $filename";
		}
		else {
			$command = 'cp';
			system "cp $location $filename";
		}
	}
	if ($compression){
		if (!-e $filename.$compression){
			# something went wrong when copying the file
			die "ERROR: could not $command $location to $filename.$compression\n";
		}
		else {
			if ($compression =~ m/^t/){
				system "tar xf $filename.$compression";
			}
			elsif ($compression =~ m/^g/){
				system "gunzip $filename.$compression";
			}
			elsif ($compression =~ m/^z/){
				system "unzip $filename.$compression";
			}
			if (!-e $filename){
				# this compression type is not currently supported
				die "ERROR: could not extract $filename.$compression to $filename\n";
			}
		}
	}
	if (!-e $filename){
		# something went wrong when copying the file
		die "ERROR: could not $command $location to $filename\n";
	}
	if (!$type){
		# TODO: infer type properly
		if ($filename =~ m/\.([^\.])$/){
			$type = $1;
			warn "WARNING: no type specified for $filename from $location, inferring '$type' based on file extension\n";
		
		}
		else {
			die "ERROR: no type specified for $filename from $location, unable to infer based on file extension\n";
		}
	}
	if (defined $new_name){
		system "mv $filename $new_name";
		if (!-e $new_name){
			die "ERROR: could not move $filename from $location to $new_name\n";
		}
		$filename = $new_name;
	}
	return ($filename,$type);
}

sub core_db_host_connect {
	my $params = shift;
	my $dsn = "DBI:mysql:host=$params->{'DATABASE_CORE'}{'HOST'};port=$params->{'DATABASE_CORE'}{'PORT'}";
	my $dbh = DBI->connect($dsn,"$params->{'DATABASE_CORE'}{'RW_USER'}","$params->{'DATABASE_CORE'}{'RW_PASS'}") || die "ERROR: unable to connect to [DATABASE_CORE] HOST ".$params->{'DATABASE_CORE'}{'HOST'}." using provided settings";
	return $dbh;
}

sub clone_ensembl_code {
	my $params = shift;
	if (!-d $params->{'ENSEMBL'}{'LOCAL'}){
		system 'mkdir -p '.$params->{'ENSEMBL'}{'LOCAL'};
		if (!-d $params->{'ENSEMBL'}{'LOCAL'}){
			die 'ERROR: unable to create directory [ENSEMBL] LOCAL '.$params->{'ENSEMBL'}{'LOCAL'}."\n";
		}
	}
	if (!-d $params->{'ENSEMBL'}{'LOCAL'}.'/ensembl'){
		system 'git clone -b '.$params->{'ENSEMBL'}{'BRANCH'}.' '.$params->{'ENSEMBL'}{'ENSEMBL'}.' '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl';
		if (!-d $params->{'ENSEMBL'}{'LOCAL'}.'/ensembl'){
			die 'ERROR: unable to checkout [ENSEMBL] code BRANCH '.$params->{'ENSEMBL'}{'BRANCH'}.' from ENSEMBL repository '.$params->{'ENSEMBL'}{'ENSEMBL'}."\n";
		}
	}
	if (!-d $params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-production'){
		system 'git clone -b '.$params->{'ENSEMBL'}{'BRANCH'}.' '.$params->{'ENSEMBL'}{'PRODUCTION'}.' '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-production';
		if (!-d $params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-production'){
			die 'ERROR: unable to checkout [ENSEMBL] code BRANCH '.$params->{'ENSEMBL'}{'BRANCH'}.' from PRODUCTION repository '.$params->{'ENSEMBL'}{'PRODUCTION'}."\n";
		}
	}
	if (!-d $params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline'){
		system 'git clone -b master '.$params->{'ENSEMBL'}{'PIPELINE'}.' '.$params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline';
		if (!-d $params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline'){
			die 'ERROR: unable to checkout [ENSEMBL] code from PIPELINE repository '.$params->{'ENSEMBL'}{'PIPELINE'}."\n";
		}
	}
	if (!-e $params->{'ENSEMBL'}{'LOCAL'}.'/ensembl/sql/table.sql'){
		die "ERROR: unable to find an sql file in the [ENSEMBL] ENSEMBL LOCAL directory ".$params->{'ENSEMBL'}{'LOCAL'}."/ensembl/sql/\n";
	}
	if (!-e $params->{'ENSEMBL'}{'LOCAL'}.'/ensembl-pipeline/sql/table.sql'){
		die "ERROR: unable to find an sql file in the [ENSEMBL] PIPELINE LOCAL directory ".$params->{'ENSEMBL'}{'LOCAL'}."/ensembl-pipeline/sql/\n";
	}
	return 1;
}

sub load_ini {
	my $ini_file = shift;
	open INI,$ini_file || die "ERROR: unable to open file $ini_file\n",usage(),"\n";
	my %params;
	my %sections = ( 
		'ENSEMBL' =>	{ 	'ENSEMBL' => 1,
							'PRODUCTION' => 1,
							'BRANCH' => 1,
							'PIPELINE' => 1,
							'LOCAL' => 1
						},
		'DATABASE_CORE' =>	{ 	'NAME' => 1,
								'HOST' => 1,
								'PORT' => 1,
								'RW_USER' => 1,
								'RW_PASS' => 1,
								'RO_USER' => 1,
								'RO_PASS' => 1
							},
		'DATABASE_TAXONOMY' => {	'NAME' => 1,
									'HOST' => 1,
									'PORT' => 1,
									'RO_USER' => 1,
									'RO_PASS' => 1
								},
		'DATABASE_TEMPLATE' => 	{	'NAME' => 1,
									'HOST' => 1,
									'PORT' => 1,
									'RO_USER' => 1,
									'RO_PASS' => 1
								},
		'META' =>	{	'PRODUCTION_NAME' => 1,
						'SCIENTIFIC_NAME' => 1,
						'TAXONOMY_ID' => 1,
						'ASSEMBLY.NAME' => 1
					},
		'FILES' => 	{	'GFF' => 1,
						'SCAFFOLD' => [ 'CONTIG' ],
						'CONTIG' => [ 'SCAFFOLD']
					},
		'GENE_DESCRIPTIONS' =>	{	'SOURCE' => 1,
									'SECTION' => 1
								},
		'GENE_NAMES' =>	{	'SOURCE' => 1,
							'SECTION' => 1
						},
		'XREFS' =>	{	
					}
		
		);
	my $section;
	while (<INI>){
		chomp;
		if (/^\s*\[(.+)\]\s*$/){
			$section = $1;
		}
		elsif (/^\s*(.+?)\s*=\s*(.+?)\s*$/) {
			my $key = $1;
			my $value = $2;
			if ($value =~ m/^\[\s*(.+)\s*\]/){
				my @value = split /\s+/,$1;
				$value = \@value;
			}
			$params{$section}{$key} = $value;
		}
	}
	my $warnings = 0;
	foreach $section (keys %sections){
		if (scalar keys %{$sections{$section}} > 0){
			if (!$params{$section}){
				# warn missing section
				warn "WARNING: missing section [$section]\n";
				$warnings++;
			}
			foreach my $subsection (keys %{$sections{$section}}){
				if ($sections{$section}{$subsection} == 1){
					if (!$params{$section}{$subsection}){
						# warn missing subsection
						warn "WARNING: missing subsection [$section] $subsection\n";
						$warnings++;
					}
				}
				else {
					if (!$params{$section}{$subsection}){
						# look for alternate section
						for (my $i = 0; $i < @{$sections{$section}{$subsection}}; $i++){
							if ($params{$section}{$sections{$section}{$subsection}->[$i]}){
								last;
							}
						}
						my $str = join (' or ',@{$sections{$section}{$subsection}});
						warn "WARNING: section [$section] must have at least one subsection of type $subsection or $str\n";
						$warnings++;
					}
				}
			}
		}
	}
	if ($warnings > 0){
		die "ERROR: unable to parse ini file $ini_file without warnings\n";
	}
	return \%params;
}

sub usage {
	return "USAGE: coming soon...";
}
