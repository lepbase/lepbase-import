#!/usr/bin/perl -w

use strict;
use Ensembl_Import;

## load parameters from an INI-style config file for maximal ensemblism
## check that all required parameters have been defined in the config file
my %params;
my $params = \%params;
my $ini_file = $ARGV[0] || die "ERROR: you must specify an ini file\n",usage(),"\n";
load_ini($params,$ini_file);

## connect to core database
my $dbh = core_db_connect($params);

## download/obtain files using methods suggested by file paths and extensions
my %infiles;
foreach my $subsection (keys %{$params->{'FILES'}}){
	($infiles{$subsection}{'name'},$infiles{$subsection}{'type'}) = fetch_file($params->{'FILES'}{$subsection});
}

## truncate database tables if option is specified

if ($params->{'MODIFY'}{'TRUNCATE_GENE_TABLES'}){
	truncate_gene_tables($dbh);
}

## load gff into ensembl
gff_to_ensembl($infiles{'GFF'}{'name'}.".sorted.gff",$dbh,$params);

count_rows($dbh,qw( gene transcript translation exon));

## TODO: use API to generate fasta files of gene, transcript, protein, etc sequences
##		 use naming convention required by sequenceserver

## TODO: compare generated sequence files with any provider files


## TODO: load other xref files (iprscan, blastp, etc) into database
##       check format before loading
##       add descriptions if specified


sub usage {
	return "USAGE: perl -I /path/to/dir/containing/Ensembl_Import.pm /path/to/gene_model_import.pl ini_file";
}


