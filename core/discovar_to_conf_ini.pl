#!/usr/bin/perl

use strict;
use warnings;

while (<>) {
    chomp;
    my ($scientific_name, $taxid, $assembly_name, $date, $provider_name, $provider_url, $dir) = split /\t/;
    my $display_name    = "$scientific_name $assembly_name";
    my $production_name =  $display_name;
    $production_name =~ s/ /_/g ;
    my $db_name = lc($production_name) . "_core_27_80_1";
    open  INI, ">../../lepbase-ensembl/conf/ini-files/$production_name.ini" or die $!;
    print INI  "
[general]
#ENSEMBL_CHROMOSOMES     = [ ]
SPECIES_RELEASE_VERSION = 1
ENSEMBL_GENOME_SIZE     = 0.5
[databases]
DATABASE_CORE = $db_name
DATABASE_USERDATA  = userdata
[DATABASE_USERDATA]
[ENSEMBL_STYLE]
[ENSEMBL_EXTERNAL_URLS]
[ENSEMBL_SPECIES_SITE]
[SPECIES_DISPLAY_NAME]
[ENSEMBL_EXTERNAL_DATABASES]
[ENSEMBL_EXTERNAL_INDEXERS]
[S4_PROTEIN]
[S4_PROTEIN_STRUCTURE]
[S4_LITERATURE]
[SAMPLE_DATA]
LOCATION_PARAM    =
LOCATION_TEXT     =
GENE_PARAM        =
GENE_TEXT         =
TRANSCRIPT_PARAM  =
TRANSCRIPT_TEXT   =
SEARCH_TEXT       =
"
}