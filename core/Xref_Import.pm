#!/usr/bin/perl -w

use strict;
use DBI;

my %external_db_ids;
my %genes;
my %hits;

sub read_blastp {
	my ($dbh,$params) = @_;
	my $external_db_id = $params->{'XREF'}{'BLASTP'}->[0];
	# TODO make external_db if not exists
	# TODO check that blastp was run with the correct command to give expected column output
	my %transcripts;
	while (<>){
		chomp;
		my ($name,@row) = split /\t/;
		next if $transcripts{$name};
		$transcripts{$name} = 1;
		my $transcript_id = transcript_id($dbh,$name);
		my $acc = my $disp = $row[0];
		$acc =~ s/^sp\|(.*?)\|.*/$1/; # if accession is of form sp|XXX|YYY, extract XXX 
		my $desc = $row[13] || undef;
		my $xref_id = xref_id($dbh,$external_db_id,$acc,$disp,$desc,'SEQUENCE_MATCH');
		my $blastp_db = $params->{'XREF'}{'BLASTP'}->[1];
		my $analysis_id = analysis_id($dbh,"BLASTP",$blastp_db); # analysis_id expects [ dbconnection, logic_name, db ]
		my $object_xref_id = object_xref_id($dbh,$xref_id,$transcript_id,$analysis_id);
		my $nident = $row[2] * $row[1] / 100;
		my $xid = $nident / $row[12] * 100;
		my $eid = $nident / $row[11] * 100;
		my $xstart = $row[7] * 3 - 2;
		my $xend = $row[8] * 3 - 2;
		my $estart = $row[5] * 3 - 2;
		my $eend = $row[6] * 3 - 2;
		my $cigar_line = btop2cigar($row[14]);
		my $score = $row[10];
		my $evalue = $row[9];
		identity_xref2($dbh,$object_xref_id,$xid,$eid,$xstart,$xend,$estart,$eend,$cigar_line,$score,$evalue);
		if ($eid > 40 && $params->{'XREF'}{'BLASTP'}[2]){
			my $gene_id = update_description($dbh,$name,$acc,$desc,$params->{'XREF'}{'BLASTP'}[2]);
		}
	}
	return 1;
}


sub read_iprscan {
	my ($dbh,$params) = @_;
	my $external_db_id = 1200;
	while (<>){
		chomp;
		my ($name,$hash,$protein_length,$analysis,$hitname,$desc,$start,$end,$evalue,@row) = split /\t/;
		my ($translation_id,$transcript_id,$gene_id) = translation_id($dbh,$name);
		die "ERROR: no translation_id for $name\n" unless $translation_id;
		my $analysis_id = analysis_id($dbh,$analysis,$analysis); # analysis takes logic_name and db but for interproscan we seem to have 
		my $ipr = ipr($dbh,$hitname);
		if ($ipr){
			$hits{$ipr}{'count'}++;
			$hits{$ipr}{'desc'} = $desc if !$hits{$ipr}{'desc'} || length $hits{$ipr}{'desc'} < length $desc;
			$hits{$ipr}{'genecount'}++ unless $genes{$ipr}{$gene_id};
			$genes{$ipr}{$gene_id}++;
			my $xref_id = xref_id($dbh,$external_db_id,$ipr,$ipr,$desc,'SEQUENCE_MATCH');
			my $object_xref_id = object_xref_id($dbh,$xref_id,$transcript_id,$analysis_id);
		}
		$hitname =~ s/G3DSA://;
		$evalue = $evalue ne '-' ? $evalue : 'NULL';
		protein_feature($dbh,$translation_id,$start,$end,$hitname,$analysis_id,$evalue,$desc);
	}
	return \%hits;
}


sub make_IPtop500_table {
	my ($hits,$params) = @_;
	open HTML,">summary/stats_".ucfirst($params->{'META'}{'SPECIES.PRODUCTION_NAME'})."_IPtop500.html";
print HTML "<table class=\"ss tint fixed data_table no_col_toggle\">
  <colgroup>
    <col width=\"10%\">
    <col width=\"50%\">
    <col width=\"20%\">
    <col width=\"20%\">
  </colgroup>
  <thead>
    <tr><th colspan=\"1\" rowspan=\"1\" class=\"sort_numeric sorting\">No.</th><th colspan=\"1\" rowspan=\"1\" class=\"sort_html sorting\">InterPro name</th><th colspan=\"1\" rowspan=\"1\" class=\"sort_numeric sorting\">Number of Genes</th><th colspan=\"1\" rowspan=\"1\" class=\"sort_position_html sorting\">Number of ensembl hits</th></tr>
  </thead>
  <tbody>
";
	my $bg = 1;
	my $ctr = 1;
	foreach my $ipr (sort { $hits{$b}{'genecount'} <=> $hits{$a}{'genecount'} } keys %hits){

print HTML "    <tr class=\"bg$bg\">
      <td class=\"bold\">$ctr</td>
      <td><a href=\"http://www.ebi.ac.uk/interpro/entry/$ipr\">$ipr</a><br>$hits{$ipr}{'desc'}</td>
      <td><a href=\"/".$params->{'META'}{'SPECIES.PRODUCTION_NAME'}."/Location/Genome?ftype=Domain;id=$ipr\">$hits{$ipr}{'genecount'}</a></td>
      <td>$hits{$ipr}{'count'}</td>
    </tr>
";
		$bg = 1 ? 2 : 1;
		$ctr++;
	}

print HTML "  </tbody>
</table>
";
	return 1;
}

sub identity_xref2 {
	my ($dbh,$object_xref_id,$xid,$eid,$xstart,$xend,$estart,$eend,$cigar_line,$score,$evalue) = @_;
	
	# test to see if this object_xref already has an identity_xref
	my $sth_idxref = $dbh->prepare("SELECT object_xref_id FROM identity_xref WHERE object_xref_id = $object_xref_id");
	$sth_idxref->execute;
	return if $sth_idxref->rows > 0;
	
	$dbh->do("INSERT INTO identity_xref (object_xref_id,xref_identity,ensembl_identity,xref_start,xref_end,ensembl_start,ensembl_end,cigar_line,score,evalue)
					 VALUES (".$object_xref_id
					 		.",".$xid
					 		.",".$eid
					 		.",".$xstart
					 		.",".$xend
					 		.",".$estart
					 		.",".$eend
					 		.",".$dbh->quote($cigar_line)
					 		.",".$score
					 		.",".$evalue
					 		.")");

}

sub update_description {
	my ($dbh,$name,$acc,$desc,$source) = @_;
	
	# test to see if the gene for this transcript already has a description
	my $sth = $dbh->prepare("SELECT g.gene_id,g.description FROM gene AS g JOIN transcript AS t ON g.gene_id = t.gene_id WHERE t.stable_id LIKE ".$dbh->quote($name));
	$sth->execute;
	return undef if $sth->rows == 0;
	
	my $values = $sth->fetchrow_arrayref();
	my $gene_id = $values->[0];
	my $description = $values->[1] || 'NULL';

	return $gene_id unless $description eq 'Unknown function' or $description eq 'NULL' or $description =~ m/Source:UniProtKB\/TrEMBL/;
	
	$desc .= " [Source:$source;Acc:" . $acc . "]";
	$dbh->do("UPDATE gene SET description = ".$dbh->quote($desc)." WHERE gene_id = $gene_id");
	
	return $gene_id;
  	
}

sub btop2cigar {
	my $btop = shift;
	my @arr = split /(\d+)/,$btop;
	my @exp;
	for (my $i = 0; $i < @arr; $i++){
		if ($arr[$i] =~ m/\d/){
			push @exp,$arr[$i];
		}
		else {
			push @exp,split/(.{2})/,$arr[$i];
		}
	}
	my $cigar = '';
	my ($match,$ins,$del);
	for (my $i = 0; $i < @exp; $i++){
		if ($exp[$i] =~ m/\d/){
			$cigar .= $del.'D' if $del;
			$del = undef;
			$cigar .= $ins.'I' if $ins;
			$ins = undef;
			$match += $exp[$i];
		}
		elsif ($exp[$i] =~ m/\w\w/){
			$cigar .= $del.'D' if $del;
			$del = undef;
			$cigar .= $ins.'I' if $ins;
			$ins = undef;
			$match++;
		}
		elsif ($exp[$i] =~ m/^-/){
			$cigar .= $match.'M' if $match;
			$match = undef;
			$cigar .= $del.'D' if $del;
			$del = undef;
			$ins++;
		}
		elsif ($exp[$i] =~ m/-$/){
			$cigar .= $match.'M' if $match;
			$match = undef;
			$cigar .= $ins.'I' if $ins;
			$ins = undef;
			$del++;
		}
	}
	$cigar .= $match.'M' if $match;
	$cigar .= $del.'D' if $del;
	$cigar .= $ins.'I' if $ins;
	return $cigar;
}

sub transcript_id {
	my ($dbh,$name) = @_;
	my $transcript_id;
	my $sth_transcript = $dbh->prepare("SELECT transcript_id FROM transcript WHERE stable_id LIKE ".$dbh->quote($name));
	$sth_transcript->execute;
	if ($sth_transcript->rows > 0){
		$transcript_id = $sth_transcript->fetchrow_arrayref()->[0];
	}
	else {
		my $sth_translation = $dbh->prepare("SELECT transcript_id FROM translation WHERE stable_id LIKE ".$dbh->quote($name));
		$sth_translation->execute;
		if ($sth_translation->rows > 0){
			$transcript_id = $sth_translation->fetchrow_arrayref()->[0];
		}
	}
	return $transcript_id;
}

sub translation_id {
	my ($dbh,$name) = @_;
	my @array;
	my $sth_translation = $dbh->prepare("SELECT tl.translation_id, tl.transcript_id, tc.gene_id FROM translation AS tl JOIN transcript AS tc ON tl.transcript_id = tc.transcript_id WHERE tl.stable_id LIKE ".$dbh->quote($name));
	$sth_translation->execute;
	if ($sth_translation->rows > 0){
		@array = $sth_translation->fetchrow_array();
	}
	return @array;
}

sub analysis_id {
	my ($dbh,$logic_name,$db) = @_;
	my $analysis_id;
	my $sth = $dbh->prepare("SELECT analysis_id FROM analysis WHERE logic_name LIKE ".$dbh->quote($logic_name)." AND db LIKE ".$dbh->quote($db));
	$sth->execute;
	if ($sth->rows > 0){
		$analysis_id = $sth->fetchrow_arrayref()->[0];
	}
	else {
		$dbh->do("INSERT INTO analysis (logic_name,db)
						VALUES (".$dbh->quote($logic_name)
								.",".$dbh->quote($db)
						 		.")");
		$sth->execute;
		$analysis_id = $sth->fetchrow_arrayref()->[0];
		my $web_data = $dbh->quote("{"."'type' "."="."> "."'domain'"."}");
		$web_data = 'NULL' if $logic_name eq 'Phobius' || $logic_name eq 'SignalP_EUK' || $logic_name eq 'TMHMM';
		$dbh->do("INSERT INTO analysis_description (analysis_id,description,display_label,displayable,web_data)
						VALUES (".$analysis_id
								.",".$dbh->quote($logic_name." predictions from InterPro Scan")
								.",".$dbh->quote($logic_name)
								.",".1
								.",".$web_data
						 		.")");
	}
	return $analysis_id;
}

sub ipr {
	my ($dbh,$name) = @_;
	my $ipr;
	my $sth = $dbh->prepare("SELECT interpro_ac FROM interpro WHERE id LIKE ".$dbh->quote($name));
	$sth->execute;
	if ($sth->rows > 0){
		$ipr = $sth->fetchrow_arrayref()->[0];
	}
	return $ipr;
}

sub xref_id {
	# lookup any names/ids in the xrefs table
	# if no matches, insert a new xref entry
	
	my ($dbh,$external_db_id,$accession,$display,$description,$info_type) = @_;
	
	my $xref_id;
	$accession = $dbh->quote($accession);
	$display = $display ? $dbh->quote($display) : $dbh->quote($accession);
	$description = $description ? $dbh->quote($description) : 'NULL';
	my $sth_xref = $dbh->prepare("SELECT xref_id FROM xref WHERE dbprimary_acc LIKE $accession AND display_label LIKE $display AND external_db_id = ".$external_db_id);
	$sth_xref->execute;
	if ($sth_xref->rows > 0){
		$xref_id = $sth_xref->fetchrow_arrayref()->[0];
	}
	else {
		$dbh->do("INSERT INTO xref (external_db_id,dbprimary_acc,display_label,description,info_type)
						VALUES (".$external_db_id
								.",".$accession
						 		.",".$display
						 		.",".$description
						 		.",".$dbh->quote($info_type)
						 		.")");
		$sth_xref->execute;
		$xref_id = $sth_xref->fetchrow_arrayref()->[0];
	}
	return $xref_id;
}

sub object_xref_id {
	my ($dbh,$xref_id,$transcript_id,$analysis_id) = @_;
	my $object_xref_id;
	my $sth_oxref = $dbh->prepare("SELECT object_xref_id FROM object_xref WHERE ensembl_id = $transcript_id AND ensembl_object_type LIKE ".$dbh->quote('Transcript')." AND analysis_id = $analysis_id AND xref_id = $xref_id");
	$sth_oxref->execute;
	if ($sth_oxref->rows > 0){
		$object_xref_id = $sth_oxref->fetchrow_arrayref()->[0];
	}
	else {
		$dbh->do("INSERT INTO object_xref (xref_id,ensembl_id,ensembl_object_type,analysis_id)
						VALUES (".$xref_id
								.",".$transcript_id
	  					 		.",".$dbh->quote('Transcript')
	  					 		.",".$analysis_id
	  					 		.")");
	    $sth_oxref->execute;
	    $object_xref_id = $sth_oxref->fetchrow_arrayref()->[0];
	}
	return $object_xref_id;
}

sub protein_feature {
	my ($dbh,$translation_id,$start,$end,$hitname,$analysis_id,$evalue,$desc) = @_;
	
	# test to see if this protein_feature already exists
	my $sth = $dbh->prepare("SELECT protein_feature_id FROM protein_feature WHERE translation_id = $translation_id AND analysis_id = $analysis_id AND seq_start = $start AND seq_end = $end AND hit_name LIKE ".$dbh->quote($hitname));
	$sth->execute;
	return if $sth->rows > 0;
	
	$dbh->do("INSERT INTO protein_feature (translation_id,seq_start,seq_end,hit_name,analysis_id,evalue,hit_description)
						 VALUES (".$translation_id
					 		.",".$start
					 		.",".$end
					 		.",".$dbh->quote($hitname)
					 		.",".$analysis_id
					 		.",".$evalue
					 		.",".$dbh->quote($desc)
					 		.")");
	
}

1;