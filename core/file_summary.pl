#!/usr/bin/perl -w

use strict;
use Ensembl_Import;
use Cwd 'abs_path';
use File::Basename;

## find the full path to the directory that this script is executing in
my $dirname  = dirname(abs_path($0));

## load parameters from an INI-style config file
## check that all required parameters have been defined in the config file
die "ERROR: you must specify at least one ini file\n",usage(),"\n" unless $ARGV[0];
my %params;
my $params = \%params;
while (my $ini_file = shift @ARGV){
	load_ini($params,$ini_file);
}

## checkout ensembl repositories
clone_ensembl_code($params);

## download/obtain files using methods suggested by file paths and extensions
my %infiles;
foreach my $subsection (keys %{$params->{'FILES'}}){
	($infiles{$subsection}{'name'},$infiles{$subsection}{'type'}) = fetch_file($params->{'FILES'}{$subsection});
	
}

# split gff to extract FASTA sequence if necessary
if ($params{'GFF'}{'SPLIT'}){
	split_gff($params,\%infiles);
}


## generate summaries of fasta, agp, gff files
## TODO: convert summary files to graphs/tables and embed in html
##       create file with example lines to help with parsing?
my %stats;
foreach my $file (keys %infiles){
	if ($file =~ m/(:?SCAFFOLD|CONTIG)/ && $infiles{$file}{'type'} eq 'fas'){
		## calculate scaffold stats and write to file
		print STDERR "Calculating summary statistics on [FILES] $file $infiles{$file}{'name'}\n";
		my $tmp_stats = fasta_file_summary($params,$infiles{$file},$file);
		foreach my $key (keys %{$tmp_stats}){
			next if $file !~ m/SCAFFOLD/ && $stats{$key};
			$stats{$key} = $tmp_stats->{$key};
		}
	}
	elsif ($infiles{$file}{'type'} eq 'agp'){
		## calculate scaffold stats and write to file
		print STDERR "Calculating summary statistics on [FILES] $file $infiles{$file}{'name'}\n";
		my $tmp_stats = agp_file_summary($params,$infiles{$file},$file);
		foreach my $key (keys %{$tmp_stats}){
			next if $file !~ m/SCAFFOLD/ && $stats{$key};
			$stats{$key} = $tmp_stats->{$key};
		}
		
	}
	elsif ($infiles{$file}{'type'} eq 'gff'){
		## generate feature summary and write to file
		print STDERR "Calculating summary statistics on [FILES] $file $infiles{$file}{'name'}\n";
		gff_feature_summary($params,$infiles{'GFF'}->{'name'});
	}
}

## generate template about_production_name.html page
about_page($params);

## generate stats_production_name.html page - IPTop500 will not exist yet but create link anyway
stats_page($params,\%stats);

## generate production_name_assembly.html page with assembly visualisation (and description)
assembly_page($params,\%stats);


sub usage {
	return "USAGE: perl -I /path/to/dir/containing/Ensembl_Import.pm /path/to/gene_model_import.pl ini_file";
}

