#!/usr/bin/perl -w

use strict;
use Bio::EnsEMBL::DBSQL::DBAdaptor;
use Getopt::Long qw(:config pass_through no_ignore_case);

my ($species_shortcode, $dbname, $source,$translation_id) = ("","","",0);
GetOptions (
  "short:s" => \$species_shortcode,
  "dbname:s" => \$dbname,
  "source:s" => \$source,
  "translation" => \$translation_id,
);

die "Usage: extract_proteins_transcripts.pl --short CSUP --dbname csuppressalis_ANCD01_1 --source http://agripestbase. --translation\n" unless $species_shortcode and $dbname and $source;

=head1

extract_proteins_transcripts.pl

USAGE:
extract_proteins_transcripts.pl --short CSUP --dbname csuppressalis_ANCD01_1 --source http://agripestbase. --translation

or

echo '
extract_proteins_transcripts.pl --short BANY --dbname banynana_nBa_0_1_1 --source bicyclus.org 2>BANY.err
extract_proteins_transcripts.pl --short BMOR --dbname bmori_ASM15162v1_2 --source ensembl 2>BMOR.err
extract_proteins_transcripts.pl --short CSUP --dbname csuppressalis_ANCD01_1 --source http://agripestbase. 2>CSUP.err
extract_proteins_transcripts.pl --short DPLX --dbname dplexippus_DanPle_1_0_2 --source MonarchBase 2>DPLX.err
extract_proteins_transcripts.pl --short HMEL --dbname hmel_Hmel1_2 --source ensembl 2>HMEL.err
extract_proteins_transcripts.pl --short MCIN --dbname mcin_MelCinx1_0_2 --source ensembl 2>MCIN.err
extract_proteins_transcripts.pl --short MSEX --dbname msexta_Msexta_1_0_1 --source http://agripestbase 2>MSEX.err
extract_proteins_transcripts.pl --short PINT --dbname pinterpunctella_PRJEB4874_1 --source cgr.liv.ac.uk 2>PINT.err
extract_proteins_transcripts.pl --short PNAP --dbname pnapi_DAS5_1 --source ChrisWheatLab 2>PNAP.err
extract_proteins_transcripts.pl --short PXYL --dbname pxylostella_AHIO01_1 --source DBM-DB 2>PXYL.err
' | parallel

=cut


my $transcript_filename   = $species_shortcode . ".fna";
my $protein_filename      = $species_shortcode . ".faa";
my $bounded_exon_filename = $species_shortcode . ".faa.bounded";

print STDERR localtime . " START species:$species_shortcode\tdbname:$dbname\tsource:$source\n";

my $dba = Bio::EnsEMBL::DBSQL::DBAdaptor->new(
    -user   => 'ensro',
    -dbname => $dbname,
    -host   => 'flowers.bio.ed.ac.uk',
    -port   => 3307,
    -driver => 'mysql'
);

my $gene_adaptor = $dba->get_GeneAdaptor();

my $genes = $gene_adaptor->fetch_all_by_source($source);

open TRANSCRIPT,   ">$transcript_filename"   or die $!;
open PROTEIN,      ">$protein_filename"      or die $!;
open BOUNDED_EXON, ">$bounded_exon_filename" or die $!;

while ( my $gene = shift @{$genes} ) {
    my $pep                   = '';
    my $longest_peptide       = '';
    my $bounded_exon          = '';
    my $longest_transcript    = '';
    my $longest_transcript_id = '';

    foreach my $transcript (@{ $gene->get_all_Transcripts }) {
        if ( $transcript->translation() ) {
        
            print STDERR "              START transcript:" . $transcript->stable_id() . "\n";
        
            $pep = $transcript->translate()->seq;
            # print $pep . "\n";
            if ( length($pep) > length($longest_peptide) ) {
                $longest_peptide = $pep;
                $bounded_exon = _prepare_exon_sequences($transcript,$pep);
                $longest_transcript = $transcript->translateable_seq();
                $longest_transcript_id = $transcript->stable_id();
                if ($translation_id == 1) {
                    $longest_transcript_id = $transcript->translation()->stable_id();
                }
            }
        }
    }
    print PROTEIN      ">$species_shortcode|$longest_transcript_id\n$longest_peptide\n"    if $longest_peptide;
    print BOUNDED_EXON ">$species_shortcode|$longest_transcript_id\n$bounded_exon\n"       if $bounded_exon;
    print TRANSCRIPT   ">$species_shortcode|$longest_transcript_id\n$longest_transcript\n" if $longest_transcript;
}

sub _prepare_exon_sequences {
	# based on Bio::EnsEMBL::Compara::SeqMember->_prepare_exon_sequences()
	my ($transcript,$sequence) = @_;
	my $sequence_exon_bounded;
	my @exons = @{$transcript->get_all_translateable_Exons};
	# @exons probably doesn't match the protein if there are such edits
	my @seq_edits = @{$transcript->translation->get_all_SeqEdits('amino_acid_sub')};
	push @seq_edits, @{$transcript->get_all_SeqEdits('_rna_edit')};

	if (((scalar @exons) <= 1) or (scalar(@seq_edits) > 0)) {
	    $sequence_exon_bounded = $sequence;
	    return $sequence_exon_bounded;
	}

	# Otherwise, we have to parse the exons
	my %boundary_chars = (0 => 'o', 1 => 'b', 2 => 'j');
	my $left_over = $exons[0]->phase > 0 ? -$exons[0]->phase : 0;
	my @this_seq = ();
       # my @exon_sequences = ();
	foreach my $exon (@exons) {
	    my $exon_pep_len = POSIX::ceil(($exon->length - $left_over) / 3);
	    my $exon_seq = substr($sequence, 0, $exon_pep_len, '');
	    $left_over += 3*$exon_pep_len - $exon->length;
	    #printf("%s: exon of len %d -> phase %d: %s\n", $transcript->stable_id, $exon_pep_len, $left_over, $exon_seq);
	    push @this_seq, $exon_seq;
	    push @this_seq, $boundary_chars{$left_over};
	   # push @exon_sequences, scalar(@exon_sequences)%2 ? $exon_seq : lc($exon_seq);
	    die sprintf('Invalid phase: %s', $left_over) unless exists $boundary_chars{$left_over}
	}
	die sprintf('%d characters left in the sequence of %s', length($sequence), $transcript->stable_id) if $sequence;
	pop @this_seq;
	$sequence_exon_bounded = join('', @this_seq);
	#$sequence_exon_cased = join('', @exon_sequences);
	return 1;
}
