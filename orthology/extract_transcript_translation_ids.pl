#!/usr/bin/perl -w

use strict;
use Bio::EnsEMBL::DBSQL::DBAdaptor;

=head1

extract_transcript_translation_ids.pl

USAGE:
perl extract_transcript_translation_ids.pl CSUP csuppressalis_ANCD01_1 http://agripestbase.

or

echo '
perl ~/extract_transcript_translation_ids.pl banynana_nBa_0_1_1 bicyclus.org 2>BANY..err
perl ~/extract_transcript_translation_ids.pl bmori_ASM15162v1_2 ensembl 2>BMOR..err
perl ~/extract_transcript_translation_ids.pl csuppressalis_ANCD01_1 http://agripestbase. 2>CSUP..err
perl ~/extract_transcript_translation_ids.pl dplexippus_DanPle_1_0_2 MonarchBase 2>DPLX..err
perl ~/extract_transcript_translation_ids.pl hmel_Hmel1_2 ensembl 2>HMEL..err
perl ~/extract_transcript_translation_ids.pl mcin_MelCinx1_0_2 ensembl 2>MCIN..err
perl ~/extract_transcript_translation_ids.pl msexta_Msexta_1_0_1 http://agripestbase 2>MSEX..err
perl ~/extract_transcript_translation_ids.pl pinterpunctella_PRJEB4874_1 cgr.liv.ac.uk 2>PINT..err
perl ~/extract_transcript_translation_ids.pl pnapi_DAS5_1 ChrisWheatLab 2>PNAP..err
perl ~/extract_transcript_translation_ids.pl pxylostella_AHIO01_1 DBM-DB 2>PXYL..err
' | parallel

=cut

my ($dbname, $source) = @ARGV;

# print STDERR localtime . " START species:$species_shortcode\tdbname:$dbname\tsource:$source\n";

my $dba = Bio::EnsEMBL::DBSQL::DBAdaptor->new(
    -user   => 'ensro',
    -dbname => $dbname,
    -host   => 'flowers.bio.ed.ac.uk',
    -port   => 3307,
    -driver => 'mysql'
);

my $gene_adaptor = $dba->get_GeneAdaptor();

my $genes = $gene_adaptor->fetch_all_by_source($source);

while ( my $gene = shift @{$genes} ) {
    foreach my $transcript (@{ $gene->get_all_Transcripts }) {
        if ( $transcript->translation() ) {
            print $transcript->stable_id() . "\t" . $transcript->translation()->stable_id() . "\n";
        }
    }
}
