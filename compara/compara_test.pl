#!/usr/bin/env perl

use strict;
use Data::Dumper;
use Bio::EnsEMBL::Compara::DBSQL::DBAdaptor;
use Bio::EnsEMBL::Compara::Graph::NewickParser;

my $cdba = new Bio::EnsEMBL::Compara::DBSQL::DBAdaptor(
    -host => 'flowers.bio.ed.ac.uk',
    -user => 'lepbase-import',
    -pass => 'lepbase-import-password',
    -port => 3307,
    -dbname => 'lepbase_compara_27_80',
#     -dbname => 'lepbase_compara_27_80',
);
my $gta   = $cdba->get_adaptor("GeneTree");
my $gtna  = $cdba->get_adaptor("GeneTreeNode");
my $seqma = $cdba->get_adaptor("SeqMember");

my $newtree = new Bio::EnsEMBL::Compara::GeneTree( -member_type => 'protein', -tree_type => 'tree', -clusterset_id => 'default',
	-method_link_species_set_id => 1, -gene_align_id => 1, -stable_id => 'OG0007470', -version => 1);

$gta->store($newtree);

#my $newick_tree = "(((Plutella_xylostella_DBM_FJ_V1.1_-_proteins_Px018018.1:0.1432190782,Plutella_xylostella_DBM_FJ_V1.1_-_proteins_Px011165.1:0.03354665321)79N:0.04849387267,Plutella_xylostella_DBM_FJ_V1.1_-_proteins_Px012932.2:1.0000005e-06)100N:0.1301292934,(((((Heliconius_melpomene_-_proteins_HMEL003249-PA:0.1093421793,Melitaea_cinxia_-_proteins_MCINX010897-PA:0.2499351291)17N:0.02253944299,Danaus_plexippus_-_proteins_EHJ64049:0.229953235)60N:0.03693885039,Lerema_accius_V1.1_-_proteins_lac300.1.mRNA:0.172089312)26N:0.0200386085,(Papilio_glaucus_V1.1_-_proteins_pgl912.20.mrna1:0.04890047465,GCF_000836215.1_Ppol_1.0_protein_XP_013141700.1:0.04335683695)100:0.08264018148)34Y:0.04017844763,(Plodia_interpunctella_v1_-_proteins_augustus_masked-scaffold233-abinit-gene-0.37-mRNA-1:0.180823198,(Bombyx_mori_-_proteins_BGIBMGA001313-TA:0.3662060642,Manduca_sexta_Msex_1.0_-_proteins_Msex2.00406-RA:0.1338694362)14N:0.02784392512)3:1.0000005e-06)100Y:0.1301292934);";
my $newick_tree = "(MCINX001692-PA:0.18765401521786653771,(EHJ65036:0.29687975328321236645,(BGIBMGA008204-TA:0.76529796648668479975,HMEL015670-PA:0.17672114003309724595)79N:0.05703889914561954821)100Y:0.06015166169858444239);";
my $newroot = Bio::EnsEMBL::Compara::Graph::NewickParser::parse_newick_into_tree($newick_tree, "Bio::EnsEMBL::Compara::GeneTreeNode");
$newroot->build_leftright_indexing;

$newroot->node_id($newtree->root_id);
$newroot->distance_to_parent($newtree->root->distance_to_parent);
$newroot->adaptor($newtree->root->adaptor);
$newroot->tree($newtree);
$newtree->{'_root'} = $newroot;

my $supertree = $gta->fetch_by_root_id(1);
$supertree->root->add_child($newroot);

foreach my $node (@{$newroot->get_all_nodes}) {
    print $node->name . "\n";
    if ($node->is_leaf) {
        my $node_name = $node->name();
        $node_name =~ s/.*_proteins?_//;
        $node->name($node_name);
        my $seqm = $seqma->fetch_by_stable_id($node->name);
        $node = bless $node, 'Bio::EnsEMBL::Compara::GeneTreeMember';
        $node->seq_member_id($seqm->seq_member_id);
        # update Compara_Import.pm to set gene_member.gene_trees to 1 when adding new gene
    } else {
        if ($node->name =~ /(\d+)(Y|N)/) {
            $node->add_tag('bootstrap',$1);
            $node->add_tag('is_dup',   $2 eq "Y" ? 1 : 0);
            $node->add_tag('node_type',$2 eq "Y" ? 'duplication' : 'speciation');
        }
    }
}

$gta->store($supertree);

my $all_nodes = $newtree->get_all_nodes;
$gtna->_store_all_tags($all_nodes);
    
