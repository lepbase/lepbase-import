#!/usr/bin/perl -w

use strict;
use Bio::EnsEMBL::DBSQL::DBAdaptor;
use Getopt::Long qw(:config pass_through no_ignore_case);

my ($species_shortcode, $dbname, $translation_id) = ("","",0);
GetOptions (
  "short:s" => \$species_shortcode,
  "dbname:s" => \$dbname,
  "translation" => \$translation_id,
);

die "Usage: extract_proteins_transcripts.pl --short CSUP --dbname csuppressalis_ANCD01_1 --translation\n" unless $species_shortcode and $dbname;

=head1

extract_proteins_transcripts.pl

USAGE:
extract_proteins_transcripts.pl --short CSUP --dbname csuppressalis_ANCD01_1 --translation

=cut

my $transcript_filename   = $species_shortcode . ".fna";
my $protein_filename      = $species_shortcode . ".faa";
my $bounded_exon_filename = $species_shortcode . ".fba";

print STDERR localtime . " START species:$species_shortcode\tdbname:$dbname\n";

my $dba = Bio::EnsEMBL::DBSQL::DBAdaptor->new(
    -user   => 'ensro',
    -dbname => $dbname,
    -host   => 'flowers.bio.ed.ac.uk',
    -port   => 3307,
    -driver => 'mysql'
);

my $gene_adaptor = $dba->get_GeneAdaptor();

my $genes = $gene_adaptor->fetch_all_by_biotype('protein_coding');

open TRANSCRIPT,   ">$transcript_filename"   or die $!;
open PROTEIN,      ">$protein_filename"      or die $!;
open BOUNDED_EXON, ">$bounded_exon_filename" or die $!;

while ( my $gene = shift @{$genes} ) {
    my $pep                   = '';
    my $longest_peptide       = '';
    my $bounded_exon          = '';
    my $longest_transcript    = '';
    my $longest_transcript_id = '';

    foreach my $transcript (@{ $gene->get_all_Transcripts }) {
        if (defined $transcript->translate() ) {
            $pep = $transcript->translate()->seq;
            $longest_peptide = $pep;
            $bounded_exon = _prepare_exon_sequences($transcript,$pep);
            $longest_transcript = $transcript->translateable_seq();
            $longest_transcript_id = $transcript->stable_id();
            if ($translation_id == 1) {
                $longest_transcript_id = $transcript->translation()->stable_id();
            }
            print PROTEIN      ">$species_shortcode\_$longest_transcript_id\n$longest_peptide\n"    if $longest_peptide;
            print BOUNDED_EXON ">$species_shortcode\_$longest_transcript_id\n$bounded_exon\n"       if $bounded_exon;
            print TRANSCRIPT   ">$species_shortcode\_$longest_transcript_id\n$longest_transcript\n" if $longest_transcript;
        }
    }
}

sub _prepare_exon_sequences {
    
    
#    my $self = shift;
#
#    # If there is the exon_bounded sequence, it is only a matter of splitting it and alternating the case
#    my $exon_bounded_seq = $self->{_sequence_exon_bounded};
#    $exon_bounded_seq = $self->adaptor->db->get_SequenceAdaptor->fetch_other_sequence_by_member_id_type($self->seq_member_id, 'exon_bounded') unless $exon_bounded_seq;
#
#    if ($exon_bounded_seq) {
#        $self->{_sequence_exon_bounded} = $exon_bounded_seq;
#        my $i = 0;
#        $self->{_sequence_exon_cased} = join('', map {$i++%2 ? lc($_) : $_} split( /[boj]/, $exon_bounded_seq));
#
#    } else {
#
#        my $sequence = $self->sequence;
        my ($transcript,$sequence) = @_;
        my $sequence_exon_bounded;
        my @exons = @{$transcript->get_all_translateable_Exons};
        # @exons probably doesn't match the protein if there are such edits
        my @seq_edits = @{$transcript->translation->get_all_SeqEdits('amino_acid_sub')};
        push @seq_edits, @{$transcript->get_all_SeqEdits('_rna_edit')};

        if (((scalar @exons) <= 1) or (scalar(@seq_edits) > 0)) {
            $sequence_exon_bounded = $sequence;
            return $sequence_exon_bounded;
        }

        # Otherwise, we have to parse the exons
        my %boundary_chars = (0 => 'o', 1 => 'b', 2 => 'j');
        my $left_over = $exons[0]->phase > 0 ? -$exons[0]->phase : 0;
        my @this_seq = ();
       # my @exon_sequences = ();
        foreach my $exon (@exons) {
            my $exon_pep_len = POSIX::ceil(($exon->length - $left_over) / 3);
            my $exon_seq = substr($sequence, 0, $exon_pep_len, '');
            $left_over += 3*$exon_pep_len - $exon->length;
            #printf("%s: exon of len %d -> phase %d: %s\n", $transcript->stable_id, $exon_pep_len, $left_over, $exon_seq);
            push @this_seq, $exon_seq;
            push @this_seq, $boundary_chars{$left_over};
           # push @exon_sequences, scalar(@exon_sequences)%2 ? $exon_seq : lc($exon_seq);
            die sprintf('Invalid phase: %s', $left_over) unless exists $boundary_chars{$left_over}
        }
        die sprintf('%d characters left in the sequence of %s', length($sequence), $transcript->stable_id) if $sequence;
        pop @this_seq;
        $sequence_exon_bounded = join('', @this_seq);
        #$sequence_exon_cased = join('', @exon_sequences);
    
}
