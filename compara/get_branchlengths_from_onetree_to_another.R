args <- commandArgs(trailingOnly = TRUE)
treefixfile = args[1];
branlenfile = args[2];

library("ape");

treefixtree <- read.tree(file=treefixfile);
branlentree <- read.tree(file=branlenfile);

m <- all.equal.phylo(branlentree,treefixtree,use.edge.length=FALSE,index.return=TRUE);

map = NULL;
tip = NULL;
len = NULL;
treefixtree$edge.length = rep(0,length(branlentree$edge.length));

for (i in 1:(length(m)/2))                   { map[m[i,1]]                         = m[i,2] };
for (i in 1:length(branlentree$edge.length)) { len[toString(branlentree$edge[i,])] = branlentree$edge.length[i] };

for (i in 1:length(branlentree$tip.label))   { treefixtree$tip.label[i]            = branlentree$tip.label[i] };
for (i in 1:length(branlentree$edge))        { treefixtree$edge[i]                 = map[treefixtree$edge[i]] };
for (i in 1:length(treefixtree$edge.length)) { treefixtree$edge.length[i]          = len[toString(treefixtree$edge[i,])] };
for (i in 1:length(treefixtree$node.label))  { treefixtree$node.label[i]           = paste0(branlentree$node.label[i],treefixtree$node.label[i]) };

write.tree(treefixtree,file=paste(treefixfile,".branlen",sep=""));
