#!/usr/bin/perl -w

use strict;
use Ensembl_Import;
use Compara_Import;

## load parameters from an INI-style config file
## check that all required parameters have been defined in the config file
die "ERROR: you must specify at least one ini file\n",usage(),"\n" unless $ARGV[0];
my %params;
my $params = \%params;
while (my $ini_file = shift @ARGV){
	load_ini($params,$ini_file);
}

## checkout ensembl repositories
clone_ensembl_code($params);

## download/obtain files using methods suggested by file paths and extensions
#my %infiles;
#foreach my $subsection (keys %{$params->{'FILES'}}){
#	($infiles{$subsection}{'name'},$infiles{$subsection}{'type'}) = fetch_file($params->{'FILES'}{$subsection});
	
#}

my $dbh = compara_db_connect($params);

opendir DIR, $params->{'ORTHOMCL'}{'PATH'} or die "Cannot open directory: $!";
my $prefix = $params->{'ORTHOMCL'}{'PREFIX'};
my @files = grep { /^$prefix\d+$/ } readdir DIR;
closedir DIR;

load_sequences($dbh,$params);

sub usage {
	return "USAGE: perl -I /path/to/dir/containing/Ensembl_Import.pm /path/to/import_homologues.pl ini_file";
}






##	define lists of taxa to treat as species sets
#	add species set information to species_set_tag
#	store species_set_id for each tag

## 	fetch meta information from list of core databases containing taxa to import
#	loop through taxa
#		obtain genome_db_id by inserting taxon db info into genome_db;
#		add appropriate information to ncbi_taxa_name and ncbi_taxa_node
#		add genome_db_id to species_set(s)

### for each taxon
##	for each gene (from core db)
#		fetch seq_region/coord_system 
#		get dnafrag_id by selecting/adding length, name, coord_system and genome_db_id to dnafrag table
#		get gene_member_id by adding information from the core gene table to the gene_member table
#		for each transcript (by gene_id from core db)
#			get seq_member_id by adding information from the core transcript table to the seq_member table and gene_member_id
#			for each translation (by transcript_id from core db)
#				get seq_member_id by adding information from the core translation table to the seq_member table and gene_member_id
#				if this is the canonical translation, add seq_member_id as canonical_member_id in gene_member table 
##	TODO: sequences that are not in a core db (e.g. Uniprot, etc. esp. outgroup sequences)
#		  add sequences to sequence table and sequence_id to seq_member table




__END__


my $dsn = "DBI:mysql:database=pxylostella_compara;host=$ENV{'REF_DB_HOST'};port=$ENV{'REF_DB_PORT'}";
my $dbh = DBI->connect($dsn,"$ENV{'REF_DB_USER'}","$ENV{'REF_DB_PASS'}");

my %taxa = ('bombyx_mori' => {	'taxon_id' => 7091,
								'assembly' => 'GCA_000151625.1',
								'assembly_default' => 1,
								'genebuild' => 'SilkDB v2.0',
								'has_karyotype' => 0,
								'is_high_coverage' => 1},
			'heliconius_melpomene' => {	'taxon_id' => 'heliconius_melpomene',
										'taxon_id' => 34740,
										'assembly' => 'Hmel1',
										'assembly_default' => 1,
										'genebuild' => '2012-03-HGC',
										'has_karyotype' => 0,
										'is_high_coverage' => 1},
			'danaus_plexippus' => {	'taxon_id' => 'danaus_plexippus',
									'taxon_id' => 13037,
									'assembly' => 'DanPle_1.0',
									'assembly_default' => 1,
									'genebuild' => '2011-11-ENA',
									'has_karyotype' => 0,
									'is_high_coverage' => 1},
			'melitaea_cinxia' => {	'taxon_id' => 'melitaea_cinxia',
									'taxon_id' => 113334,
									'assembly' => 'MelCinx1.0',
									'assembly_default' => 1,
									'genebuild' => 'Mcin_v1.0',
									'has_karyotype' => 0,
									'is_high_coverage' => 1},
			'plutella_xylostella' => {	'taxon_id' => 'plutella_xylostella',
										'taxon_id' => 51655,
										'assembly' => 'AHIO01',
										'assembly_default' => 1,
										'genebuild' => '',
										'has_karyotype' => 0,
										'is_high_coverage' => 1}
			);

my %sets = ('butterflies' => ['heliconius_melpomene', 'danaus_plexippus', 'melitaea_cinxia'],
			'moths' => ['bombyx_mori', 'plutella_xylostella'],
			'lepidoptera' => ['bombyx_mori', 'plutella_xylostella','heliconius_melpomene', 'danaus_plexippus', 'melitaea_cinxia']
			);			
			

# copy schema into new db
# mysqldump -uroot -p --no-data ensembl_compara_metazoa_23_76 > compara.sql
# mysql -uroot -p pxylostella_compara < compara.sql

# copy `ncbi_taxa_%` tables into new db
# mysqldump -uroot -p ensembl_compara_metazoa_23_76 ncbi_taxa_node ncbi_taxa_name | mysql -uroot -p pxylostella_compara

# add schema_version and schema_type to meta table
$dbh->do("INSERT IGNORE INTO meta (meta_key, meta_value) VALUES ('schema_version','76')");
$dbh->do("INSERT IGNORE INTO meta (meta_key, meta_value) VALUES ('schema_type','compara')");

# add taxa to genome_db table
my $sth = $dbh->prepare("SELECT genome_db_id FROM genome_db WHERE taxon_id = ?");
foreach my $name (keys %taxa){
	$dbh->do("INSERT IGNORE INTO genome_db (taxon_id, name, assembly, assembly_default, genebuild, has_karyotype, is_high_coverage)"
			." VALUES (".$taxa{$name}{'taxon_id'}.","
						.$dbh->quote($name).","
						.$dbh->quote($taxa{$name}{'assembly'}).","
						.$taxa{$name}{'assembly_default'}.","
						.$dbh->quote($taxa{$name}{'genebuild'}).","
						.$taxa{$name}{'has_karyotype'}.","
						.$taxa{$name}{'is_high_coverage'}
			.")");
	$sth->execute($taxa{$name}{'taxon_id'});
	$taxa{$name}{'genome_db_id'} = $sth->fetchrow_arrayref()->[0];
}

# add species sets and tags for any groups of species defined in %sets
# further sets will need to be defined as they become relevant during the compara pipeline

foreach my $set (keys %sets){
	my $query = "SELECT species_set_id, GROUP_CONCAT(name) AS species FROM species_set JOIN genome_db USING(genome_db_id) GROUP BY species_set_id HAVING";
	for  (my $i = 0; $i < @{$sets{$set}}; $i++){
		$query .= " species LIKE '\%".$sets{$set}[$i]."\%'";
		$query .= " AND" if $i + 1 < @{$sets{$set}};
	}
	$query .= " ORDER BY species_set_id";
	$sth = $dbh->prepare($query);
	$sth->execute();
	my $species_set_id;
	if ($sth->rows > 0){
		while (my $id = $sth->fetchrow_arrayref()){
			my $sth2 = $dbh->prepare("SELECT count(*) FROM species_set WHERE species_set_id = ".$id->[0]);
  			$sth2->execute();
  			my $count = $sth2->fetchrow_arrayref()->[0];
  			if ($count == scalar @{$sets{$set}}){
  				$species_set_id = $id->[0];
  				print $species_set_id,"\n";
  				last;
  			}
		}
  	}
  	if (!$species_set_id) {
  		my $tmp = $dbh->prepare("SELECT max(species_set_id) FROM species_set");
		$tmp->execute();
		if ($tmp->rows > 0){
    		$species_set_id = $tmp->fetchrow_arrayref()->[0];
    		$species_set_id++;
  		}
  		else {
  			$species_set_id = 1;
  		}
  		for  (my $i = 0; $i < @{$sets{$set}}; $i++){
			my $genome_db_id = $taxa{$sets{$set}[$i]}{'genome_db_id'};
			$dbh->do("INSERT IGNORE INTO species_set (species_set_id, genome_db_id) VALUES ($species_set_id, $genome_db_id)");
		}
		$dbh->do("INSERT IGNORE INTO species_set_tag (species_set_id, tag, value) VALUES ($species_set_id, 'name', '$set')");
	}
  	
}




__END__

my $sth = $dbh->prepare("show tables;");
$sth->execute();
while (my $ref = $sth->fetchrow_arrayref()){
	print $ref->[0],"\n";
}
