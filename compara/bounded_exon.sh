#!/bin/bash

#$ -V
#$ -cwd
#$ -o $JOB_ID.log
#$ -j y

echo '
perl -I ./ -I ../genebuild/ensembl/modules extract_proteins_transcripts.pl --short BMOR --dbname bmori_ASM15162v1_2 --source ensembl 2>BMOR.err --translation
perl -I ./ -I ../genebuild/ensembl/modules extract_proteins_transcripts.pl --short DPLX --dbname dplexippus_DanPle_1_0_2 --source MonarchBase 2>DPLX.err --translation
perl -I ./ -I ../genebuild/ensembl/modules extract_proteins_transcripts.pl --short HMEL --dbname hmel_Hmel1_2 --source ensembl 2>HMEL.err --translation
perl -I ./ -I ../genebuild/ensembl/modules extract_proteins_transcripts.pl --short MCIN --dbname mcin_MelCinx1_0_2 --source ensembl 2>MCIN.err --translation
' | parallel

